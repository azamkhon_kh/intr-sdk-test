<?php
require './autoload.php';

use Introvert\Configuration;


$date_to = '2020-06-20 00:00:01';
$date_from = '2021-09-21 00:00:01';

// Configure API key authorization: api_key
function getClients()
{
    return [
        [
            'id' => 1,
            'name' => 'intrdev',
            'api' => '23bc075b710da43f0ffb50ff9e889aed'
        ],
        [
            'id' => 2,
            'name' => 'artedegrass0',
            'api' => '23bc075b710da43f0ffb50ff9e889aed'
        ],
    ];
}

function getLeadAndBudget($date_from, $date_to, string $key): array
{
    Introvert\Configuration::getDefaultConfiguration()->setApiKey('key', $key);
    $api = new Introvert\ApiClient();
    // это успешно реальзовано id = 142
    try {
        $allLeads = $api->lead->getAll(null, [142], null, null, 250);
    } catch (Exception $e) {
        if ($e->getCode() == 401) {
            //echo 'неверный API ключ !';
            return ['code' => 401, 'budget' => 'неверный API ключ !'];
        }
        //echo 'Произошла ошибка при запросе всех сделок ( File : ' . $e->getFile() . ' | Message: ' . $e->getMessage() . ' | Line: ' . $e->getLine() . ' | Code: ' . $e->getCode() . ')';
        return ['code' => 0, 'budget' => 'Произошла ошибка при запросе всех сделок'];
    }
    //echo $allLeads['code'] == 1 ? 'Взят список сделок (количесво ' . $allLeads['count'] . ')' : "что то пошло не так " . $allLeads['message'];
    //print_r($allLeads);
    // тепер взять значение создания сделок и сравнить
    $result = array('code' => 1, 'budget' => 0, 'lead' => array());
    foreach ($allLeads['result'] as $lead) {
        if (!empty($lead['date_close']) && $date_from <= $lead['date_close'] && $lead['date_close'] <= $date_to) {
            $result['leads'][] = $lead;
            $result['budget'] += $lead['price'];
        }
    }
    return $result;
}


function addRow(array $options): string
{
    $result = '<tr>';
    foreach ($options as $key => $option) {
        $result .= '<td>' . $option . '</td>';
    }
    $result .= '</tr>';
    return $result;
}

function task1($date_to, $date_from): string
{
    $sum = 0;
    $html = '<table border="2px">
            <thead>
                    <td>Yadro ID</td>
                    <td>название клиента</td>
                    <td>сумма его успешных сделок за период ' . $date_from . ' - ' . $date_to . ' </td>
            </thead>
            <tbody>';
    //$leadAndBudget = getLeadAndBudget($date_to, $date_from);
    foreach (getClients() as $user) {
        $budget = getLeadAndBudget(strtotime($date_from), strtotime($date_to), $user['api']);
        $html .= addRow([
            'YandroID' => $user['id'],
            'name' => $user['name'],
            'budget' => $budget['budget']
        ]);
        $sum += $budget['budget'];
    }
    $html .= addRow([
        'lastRow' => 'Общая сумма',
        'sum' => $sum,
        'empty' => ''
    ]);
    // prerare+ html
    $html .= '</tbody> </table>';
    return $html;

}

echo task1($date_from, $date_to);